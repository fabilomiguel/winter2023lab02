import java.util.Scanner;
public class MethodTest{
	
	public static void main(String[] args){
		//int x = 5;
		//System.out.println(x);
		//methodOneInputNoReturn(x+10);
		//System.out.println(x);
		//methodTwoInputNoReturn(9, 4.5);
		//int z = methodTwoInputReturnInt();
		//System.out.println(z);
		//double z = sumSquareRoot(9, 5);
		//System.out.println(z);
		/*String s1 = "java";
		String s2 = "programming";
		System.out.println(s1.length());
		System.out.println(s2.length());*/
		
		//Question 11
		
		//System.out.println(SecondClass.addOne(50));
		System.out.println(SecondClass.addTwo(50));
		SecondClass sc = new SecondClass();
		System.out.println(sc.addOne(50));
	}
	
	public static void methodNoInputNoReturn(){
		
		System.out.println("I'm in a method that takes no input and returns nothing");
		int x = 20; //Declaring the variable that will be printed
		System.out.println(x);
	}

	public static void methodOneInputNoReturn(int s){
		
		System.out.println("Inside the method one input no return");
		s = s - 5;
		System.out.println(s);
	}

		public static void methodNoInputNoReturn(int a, double b) {
		
		System.out.println("Inside the method two inputs no return");
		System.out.println(a);
		System.out.println(b);
	}
	public static int methodTwoInputReturnInt() {
		
		return 5;
	}
	
	public static double sumSquareRoot(int a, int b) {
		double c = Math.sqrt(a+b);
		return c;
	}
}
	