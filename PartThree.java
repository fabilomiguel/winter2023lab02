import java.util.Scanner;
public class PartThree{
	
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter the first number");
		double a = scan.nextDouble();
		System.out.println("Please enter the second number");
		double b = scan.nextDouble();
		
		System.out.println("The sum of the numbers is :" + Calculator.add(a,b));
		System.out.println("The subtraction of the numbers is :" + Calculator.subtract(a,b));
				
		Calculator multi = new Calculator();
		System.out.println("The multiplication of the numbers is :" + multi.multiply(a,b));
		Calculator div = new Calculator();
		System.out.println("The division of the numbers is :" + div.divide(a,b));
	}
}
	