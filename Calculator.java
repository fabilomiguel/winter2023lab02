public class Calculator{
	
	public static double add(double a, double b){
		
		return a+b;
	}
	public static double subtract(double a, double b){
		
		return a-b;
	}
	public double multiply(double a, double b){
		
		return a*b;
	}
	public double divide(double a, double b){
		
		if (b != 0)
		{
			return a/b;
		}
		else if (a != 0)
		{
			return b/a;
		}
		else
		{
			return 0;
		}
	}
}